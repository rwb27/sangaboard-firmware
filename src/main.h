#ifndef MAIN_H
#include "config.h"
#include <Arduino.h>
#ifdef DEBUG_ON
#define D(x) Serial.println(x)
#else
#define D(x)
#endif

#define END_COMMAND \
    {               \
        "", NULL    \
    }
#define CHECK_END_COMMAND(C) C.command.length() > 0

struct Command
{
    String command;
    void (*run)(String);
};

uint8_t parse_arguments(char ** arguments, String, uint8_t);
extern void register_module(const Command commands[], void (*loop_fn)(void));

void get_version(String);
void get_modules(String);
#define MAIN_H
#endif